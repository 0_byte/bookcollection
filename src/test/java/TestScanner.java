import ch.qos.logback.classic.Logger;
import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.Device;
import org.home.entity.mediaCollection.FileType;
import org.home.entity.mediaCollection.MediaCollection;
import org.home.entity.mediaCollection.bmf.realization.EpubMCFile;
import org.home.entity.mediaCollection.bmf.realization.FB2MCFile;
import org.home.entity.mediaCollection.bmf.realization.PDFMCFile;
import org.home.scanner.ScanResult;
import org.home.scanner.Scanner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestScanner {
    private static final Logger logger =
            (Logger) LoggerFactory.getLogger(TestScanner.class);
    private MediaCollection mc;
    private Device device;
    private Path testPath;
    private Path tempoDir;

    private void init() {
        device = new Device();
        testPath = Paths.get("testDir");
        tempoDir = Paths.get("tempo");
        mc = new MediaCollection(device, testPath);

        logger.debug("Device id: " + device.getId());
        logger.debug("Media collection id: " + mc.getId());
    }

    public static void printScanResults(ScanResult scanResult){
        logger.trace("Found files");
        scanResult.getFoundMCFilesList()
                .stream()
                .filter(mcFile -> mcFile.getFileType() != FileType.FILE_TYPE_UNDEFINED)
                .forEach(
                        mcFile -> logger.trace(mcFile.toString())
                );
    }

    @Test
    public void testScanner() {
        init();
        Assert.assertTrue(Files.exists(testPath)
            && Files.exists(tempoDir));

        Scanner scanner = new Scanner();
        logger.debug("Scanner id " + scanner.getScanId());
        ScanResult scanResult = scanner.scanFull(mc);
        logger.debug("Scan result id: " + scanResult.getId());
        // 4 for config property handle_non_extension_file = false, 6 for true
        Assert.assertEquals("Assert files count ", 6, scanResult.getFoundMCFilesList().size());
        logger.debug("Find files: " + scanResult.getFoundMCFilesList().size());

        printScanResults(scanResult);
    }

    @Test
    public void testSingleFileCollection(){
        device = new Device();
        testPath = Paths.get("testDir/another dir/__Strugackie_A._Hromaya_Sudba.fb2");
        mc = new MediaCollection(device, testPath);

        logger.debug("Device id: " + device.getId());
        logger.debug("Media collection id: " + mc.getId());

        Scanner scanner = new Scanner();
        logger.debug("Scanner id " + scanner.getScanId());
        ScanResult scanResult = scanner.scanFull(mc);
        printScanResults(scanResult);
    }

    @Test
    public void testParseBook(){
        init();
        Assert.assertTrue(Files.exists(testPath)
                && Files.exists(tempoDir));

        Scanner scanner = new Scanner();
        logger.debug("Scanner id " + scanner.getScanId());
        ScanResult scanResult = scanner.scanFull(mc);
        logger.debug("Scan result id: " + scanResult.getId());

        Assert.assertTrue(scanResult.getCountByType(BookFileType.FILE_TYPE_PDF) == 1);

        scanResult.getFoundMCFilesList()
                .stream()
                .filter(mcFile -> mcFile.getFileType() == FileType.FILE_TYPE_READABLE
                && mcFile.getMCfile() != null)
                .forEach(mcFile -> {
                    logger.debug(mcFile.getFileName());
                    logger.debug("Is null " + (mcFile.getMCfile() == null));

                    BookFileType bookFileType = mcFile.getMCfile().getBookFileType();

                    logger.debug("book file type " + bookFileType);
                    switch (bookFileType){
                        case FILE_TYPE_PDF:
                            logger.debug("pdf file");
                            PDFMCFile pdf = (PDFMCFile) mcFile.getMCfile();
                            logger.debug("Book name " + pdf.getBookName());
                            logger.debug("Title " + pdf.getTitle());
                            logger.debug("Number of pages " + pdf.getNumberOfPages());
                            break;
                        case FILE_TYPE_FB2:
                            logger.debug("fb2 file");
                            FB2MCFile fb2Book = (FB2MCFile) mcFile.getMCfile();
                            logger.debug("Book name " +fb2Book.getBookName());
                            logger.debug("Title " + fb2Book.getTitle());
                            logger.debug("genre " + fb2Book.getGenre());
                            break;
                        case FILE_TYPE_EPUB:
                            logger.debug("epub file");
                            EpubMCFile epubMCFile = (EpubMCFile) mcFile.getMCfile();
                            logger.debug("Book name " + epubMCFile.getBookName());
                            logger.debug("Title " + epubMCFile.getTitle());
                            logger.debug("publisher " + epubMCFile.getPublisherName());
                            logger.debug("author " + epubMCFile.getAuthor());
                    }
                });
    }


}
