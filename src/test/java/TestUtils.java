import ch.qos.logback.classic.Logger;
import org.home.utils.UniquePCId;
import org.home.utils.Utils;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class TestUtils {
    private static final Logger logger =
            (Logger) LoggerFactory.getLogger(TestUtils.class);

    @Test
    public void testUniqueId() {
        String pcId = new UniquePCId().getPCId();
        System.out.println(pcId);
    }

    @Test
    public void testDurationTimeFormatter(){
        String s = Utils.timeFormatter(Duration.ofSeconds(3732));
        logger.debug("Duration " + s);
    }
}
