package org.home.scanner.visitior;

import org.home.entity.mediaCollection.FileType;
import org.home.entity.mediaCollection.MCUtils;
import org.home.entity.mediaCollection.MediaCollection;
import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.scanner.ScanResult;
import org.home.utils.FileUtils;
import org.home.utils.PropertiesHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

//TODO think about default method implementation for various scan visitors

public class FullScanFileVisitor extends SimpleFileVisitor<Path> {

    private static final Logger logger =
            LoggerFactory.getLogger(FullScanFileVisitor.class);
    private static final boolean handleNonExtensionFiles = PropertiesHandler.getPropertiesHanlder()
            .getProperty("handle_non_extension_file").equals("true");
    private static final PathMatcher pathMatcher = FileSystems.getDefault()
            .getPathMatcher("glob:{" + FileUtils.preparePathMatcherExtensions() + "}");
    private ScanResult scanResult;
    private MediaCollection mc;

    public FullScanFileVisitor(ScanResult scanResult) {
        this.scanResult = scanResult;
        mc = scanResult.getInitialMediaCollection();
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        //TODO for more "smart" implementations add all files to scan result, them check em
        try {
            if (MCUtils.isExcludeFolder(scanResult.getInitialMediaCollection(), dir)) {
                scanResult.getMessagesList()
                        .add("File was skipped, according to ignore list: " + dir.toString());
                return FileVisitResult.SKIP_SUBTREE;
            }
        } catch (IOException e) {
            onFalseVisitAction(scanResult, dir, false);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        if(attrs.isSymbolicLink()){
            scanResult.getMessagesList().add("File was skipped, symlink: " + file.toString());
            logger.debug("Skip symlink file");
        } else if(attrs.isRegularFile()){
           try {
               if(MCUtils.isExcludeFile(mc, file)){
                   scanResult.getMessagesList().add("File was skipped, according to ignore list: " + file.toString());
                   logger.debug("skip according to exclude list " + file.toString());
                   return FileVisitResult.CONTINUE;
               }

               if(pathMatcher.matches(file.getFileName())){
                   MCFile mcFile = onSuccesFileVisitAction(scanResult, file, FileType.FILE_TYPE_READABLE);
                   mcFile.setModifDate(FileUtils.getLastModifiedDate(file));
                   mcFile.setSize(FileUtils.calculateFileSize(mcFile.getFile()));
                   logger.trace("File size " + FileUtils.calculateFileSize(mcFile.getFile()));
               }else{
                   if(handleNonExtensionFiles){
                       onSuccesFileVisitAction(scanResult, file, FileType.FILE_TYPE_ATTACH);
                       scanResult.getMessagesList().add("Attach file was found" + file.toString());
                       logger.debug("Attach file was found");
                   }else{
                       onSuccesFileVisitAction(scanResult, file, FileType.FILE_TYPE_UNDEFINED);
                       scanResult.getMessagesList().add("Non book file was added as UndefinedMCFile" + file.toString());
                       logger.debug("Non book file was added as UndefinedMCFile");
                   }
               }
           }catch (IOException e){
               onFalseVisitAction(scanResult, file, true);
           }
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        onFalseVisitAction(scanResult, file, true);
        return FileVisitResult.CONTINUE;
    }

    protected static MCFile onFalseVisitAction(ScanResult scanResult, Path file, boolean isFile){
        MCFile mcFile = new MCFile(
                scanResult.getInitialMediaCollection().getId(),
                file,
                FileType.FILE_TYPE_UNREADABLE);
        mcFile.setIsReadable(false);
        mcFile.setIsFile(isFile);
        scanResult.getFoundMCFilesList().add(mcFile);
        scanResult.getMessagesList().add("Cannot visit file " + file + ", added as UnreadableMCFile");
        logger.error("Cannot visit file " + file);
        return mcFile;
    }

    protected static MCFile onSuccesFileVisitAction(ScanResult scanResult, Path file, FileType fileType){
        MCFile mcFile = new MCFile(scanResult.getInitialMediaCollection().getId(),
                file,
                fileType);
        mcFile.setIsReadable(true);
        mcFile.setIsExists(true);
        mcFile.setIsFile(true);

        String[] fileExtension = FileUtils.getFileExtension(mcFile.getFile());
        mcFile.setFileName(fileExtension[0]);
        mcFile.setExt(fileExtension[1]);

        scanResult.getFoundMCFilesList().add(mcFile);

        return mcFile;
    }
}
