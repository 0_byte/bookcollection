package org.home.scanner;

public enum ScanType {
    SCAN_TYPE_FULL, // scan and parse without compare with previous result
    SCAN_TYPE_QUICK, // check by modification date and size and parse new files
    SCAN_TYPE_SMART, // scan all directories, then compare with previous result and parse new files
    SCAN_TYPE_ONLY_SCAN, // scan only, without parsing
    SCAN_TYPE_ONLY_PARSE, // parse already scanned files
}
