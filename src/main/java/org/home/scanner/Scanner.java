package org.home.scanner;


import org.dom4j.DocumentException;
import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.FileType;
import org.home.entity.mediaCollection.MediaCollection;
import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.entity.mediaCollection.bmf.realization.EpubMCFile;
import org.home.entity.mediaCollection.bmf.realization.FB2MCFile;
import org.home.parser.EpubParser;
import org.home.parser.FB2Parser;
import org.home.parser.PDFParser;
import org.home.scanner.visitior.FullScanFileVisitor;
import org.home.utils.PropertiesHandler;
import org.home.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.home.entity.mediaCollection.BookFileType.*;

public class Scanner {

    private static final Logger logger = LoggerFactory.getLogger(Scanner.class);
    private static boolean parse_book_covers = PropertiesHandler.getPropertiesHanlder()
            .getProperty("parse_book_covers").equals("true");
    private String scanId;
    public static String separator = "=================================";

    public Scanner() {
        scanId = UUID.randomUUID().toString();
    }

    public String getScanId() {
        return scanId;
    }

    public ScanResult scanFull(MediaCollection mediaCollection) {
        ScanResult sc = new ScanResult(scanId, mediaCollection);
        sc.setScanType(ScanType.SCAN_TYPE_FULL);
        startScan(sc);
        return sc;
    }

    public ScanResult scanSmart(MediaCollection mediaCollection) {
        //TODO implement scan
        ScanResult sc = new ScanResult(scanId, mediaCollection);
        sc.setScanType(ScanType.SCAN_TYPE_SMART);
        startScan(sc);
        return sc;
    }

    public ScanResult scanQuick(MediaCollection mediaCollection) {
        //TODO implement scan
        ScanResult sc = new ScanResult(scanId, mediaCollection);
        sc.setScanType(ScanType.SCAN_TYPE_QUICK);
        return sc;
    }

    public ScanResult scanOnly(MediaCollection mediaCollection) {
        ScanResult sc = new ScanResult(scanId, mediaCollection);
        sc.setScanType(ScanType.SCAN_TYPE_ONLY_SCAN);
        return sc;
    }

    public ScanResult parseOnly(MediaCollection mediaCollection) {
        //TODO implement parse
        ScanResult sc = new ScanResult(scanId, mediaCollection);
        sc.setScanType(ScanType.SCAN_TYPE_ONLY_PARSE);
        return sc;
    }

    public ScanResult parseOnly(ScanResult scanResult) {
        //TODO implement parse
        ScanResult sc = new ScanResult(scanId, scanResult);
        sc.setScanType(ScanType.SCAN_TYPE_ONLY_PARSE);
        return sc;
    }

    private void startScan(ScanResult scanResult) {
        MediaCollection mediaCollection = scanResult.getInitialMediaCollection();
        Path mcRootFile = mediaCollection.getRootPath();

        Instant benchStartTime = startBenchMark(scanResult);

        mediaCollection.setIs_exists(Files.exists(mcRootFile, LinkOption.NOFOLLOW_LINKS));
        if (!mediaCollection.is_exists()) {
            scanResult.getMessagesList().add("Root media collection file is not exist");
            logger.error("Media collection file is not exist");

            stopBenchMark(scanResult, benchStartTime);
            scanResult.setScanResultScanned(true);
            return;
        }

        if (!Files.isReadable(mcRootFile)) {
            mediaCollection.setIs_readable(false);
            stopBenchMark(scanResult, benchStartTime);
            scanResult.setScanResultScanned(true);
            return;
        }

        if (Files.isRegularFile(mcRootFile, LinkOption.NOFOLLOW_LINKS)) {
            mediaCollection.setIs_file(true);
            mediaCollection.setIs_folder(false);
        } else if (Files.isDirectory(mcRootFile, LinkOption.NOFOLLOW_LINKS)){
            mediaCollection.setIs_file(false);
            mediaCollection.setIs_folder(true);
        }

        try {
            switch (scanResult.getScanType()) {
                case SCAN_TYPE_FULL:
                    startFullScan(scanResult);
                    parse(scanResult);
                    break;
            }
        } catch (IOException ioe) {
            // All exceptions are handled inside FileVisitor, but class require
            // implementation of catch cases
            logger.error("IO exception " + ioe.getMessage());
        }finally {
            stopBenchMark(scanResult, benchStartTime);
            scanResult.setScanResultScanned(true);
        }
    }

    private void startFullScan(ScanResult scanResult) throws IOException {
        FullScanFileVisitor fullScanFileVisitor = new FullScanFileVisitor(scanResult);
        Files.walkFileTree(
                scanResult.getInitialMediaCollection().getRootPath(),
                fullScanFileVisitor
        );
        scanResult.setScanResultScanned(true);
    }

    private void parse(ScanResult scanResult){
        List<MCFile> foundMCFilesList = scanResult.getFoundMCFilesList();

        for (MCFile mcFile: foundMCFilesList) {
            switch (mcFile.getFileType()) {
                case FILE_TYPE_READABLE:
                    try{
                        switch (determineBookFileType(mcFile)){
                            case FILE_TYPE_FB2:
                                mcFile.setMCfile(
                                        FB2Parser.parse(mcFile, parse_book_covers));
                                scanResult.increaseCount(FILE_TYPE_FB2);
                                break;
                            case FILE_TYPE_PDF:
                                mcFile.setMCfile(
                                        PDFParser.parse(mcFile, parse_book_covers));
                                scanResult.increaseCount(FILE_TYPE_PDF);
                                break;
                            case FILE_TYPE_EPUB:
                                mcFile.setMCfile(
                                        EpubParser.parse(mcFile, parse_book_covers));
                                scanResult.increaseCount(FILE_TYPE_EPUB);
                                break;
                            case FILE_TYPE_DOC:
                                scanResult.increaseCount(FILE_TYPE_DOC);
                                break;
                            case FILE_TYPE_RTF:
                                scanResult.increaseCount(FILE_TYPE_RTF);
                                break;
                            case FILE_TYPE_TXT:
                                scanResult.increaseCount(FILE_TYPE_TXT);
                                break;
                            case FILE_TYPE_BOOK_IMPLEMENTATION:
                            default:
                                scanResult.increaseCount(FILE_TYPE_BOOK_IMPLEMENTATION);
                                logger.debug("Non recognizable book format. Cannot find parser");
                        }
                    }catch (IOException | DocumentException e){
                        logger.error("Error during parsing book");
                    }
                    break;
                case FILE_TYPE_UNREADABLE:
                    logger.debug("Unreadable file, will skip parsing");
                    scanResult.increaseCount(FileType.FILE_TYPE_UNREADABLE);
                    break;
                case FILE_TYPE_UNDEFINED:
                    logger.debug("Undefined file, will skip parsing");
                    scanResult.increaseCount(FileType.FILE_TYPE_UNDEFINED);
                    break;
                case FILE_TYPE_ATTACH:
                    //TODO think about it, like attach special params
                    logger.debug("Attach file was found");
                    scanResult.increaseCount(FileType.FILE_TYPE_ATTACH);
                    break;
            }
        }

    }

    private static synchronized BookFileType determineBookFileType(MCFile mcFile){
        List<BookFileType> listOfBookValues = BookFileType.getListOfValues();
        for (BookFileType bft: listOfBookValues){
            if (Objects.equals(mcFile.getExt(), bft.toString())){
                return bft;
            }
        }
        return FILE_TYPE_BOOK_IMPLEMENTATION;
    }

    private static synchronized Instant startBenchMark(ScanResult scanResult){
        LocalDateTime startDateTime = LocalDateTime.now();
        Instant start = Instant.now();
        scanResult.setDateTimeStart(startDateTime);
        logger.debug("Scan started at: " + startDateTime.toString());
        return start;
    }

    private static synchronized void stopBenchMark(ScanResult scanResult, Instant start){
        LocalDateTime endScatDateTime = LocalDateTime.now();
        Instant stop = Instant.now();
        Duration scanTime = Duration.between(start, stop);

        scanResult.setDateTimeScanFinish(endScatDateTime);
        scanResult.setScanTime(scanTime);
        logger.debug("Duration " + scanTime);
        logger.debug("Scan finished at: " + endScatDateTime.toString());
        logger.debug("Scan duration was: " + Utils.timeFormatter(scanTime));
    }
}
