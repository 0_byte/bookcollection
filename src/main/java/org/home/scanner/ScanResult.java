package org.home.scanner;

import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.FileType;
import org.home.entity.mediaCollection.MediaCollection;
import org.home.entity.mediaCollection.bmf.MCFile;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class ScanResult {

    private String id;
    private String scanId;
    private ScanType scanType;

    private MediaCollection initialMediaCollection; // media collection that will be scanned / parsed
    private ScanResult initialScanResult; // if you want to get new scan result from existing

    private Duration scanTime;
    private Duration parseTime;
    private Duration globalTime;

    private LocalDateTime dateTimeStart;
    private LocalDateTime dateTimeScanFinish;
    private LocalDateTime dateTimeParseStart;
    private LocalDateTime dateTimeParseFinish;

    private int totalCount;
    // map that consist found file types and their amount number
    private Map<FileType, Integer> foundFilesCountMap;
    // that consist found book file types and their amount number
    private Map<BookFileType, Integer> foundBooksCountMap;

    private boolean isScanResultParsed = false; // if scan process already finished
    private boolean isScanResultScanned = false; //if parse process already finished

    private List<MCFile> foundMCFilesList; // found files, all types, include "bad" files
    private List<MCFile> excludeMCFilesList; // need to know what we scan or not
    private List<String> messagesList; // messages during scan/parse process

    private ScanResult() {}

    public ScanResult(String scanId, MediaCollection mediaCollection) {
        this.id = UUID.randomUUID().toString();
        this.scanId = scanId;
        this.initialMediaCollection = mediaCollection;
        this.init();
    }

    public ScanResult(String scanId, ScanResult scanResult) {
        this.id = UUID.randomUUID().toString();
        this.scanId = scanId;
        this.initialMediaCollection = scanResult.getInitialMediaCollection();
        this.initialScanResult = scanResult;
        this.init();
    }

    private void init() {
        foundBooksCountMap = new HashMap<>();
        foundFilesCountMap = new HashMap<>();
        foundMCFilesList = new ArrayList<>();
        excludeMCFilesList = new ArrayList<>();
        messagesList = new ArrayList<>();

        scanTime = parseTime = globalTime = Duration.ZERO;

        BookFileType.getListOfValues()
                .forEach(bookFileType ->
                        foundBooksCountMap.put(bookFileType, 0));
        FileType.getListOfValues()
                .forEach(fileType -> foundFilesCountMap.put(fileType, 0));
    }

    public void increaseCount(FileType fileType) {
        foundFilesCountMap.put(fileType,
                foundFilesCountMap.get(fileType) + 1);
    }

    public Integer getCountByType(FileType fileType) {
        return foundFilesCountMap.get(fileType);
    }

    public void increaseCount(BookFileType fileType) {
        foundBooksCountMap.put(fileType,
                foundBooksCountMap.get(fileType) + 1);
    }

    public Integer getCountByType(BookFileType fileType) {
        return foundBooksCountMap.get(fileType);
    }

    public MediaCollection getInitialMediaCollection() {
        return initialMediaCollection;
    }

    public ScanType getScanType() {
        return scanType;
    }

    public void setScanType(ScanType scanType) {
        this.scanType = scanType;
    }

    public Duration getScanTime() {
        return scanTime;
    }

    public void setScanTime(Duration scanTime) {
        this.scanTime = scanTime;
    }

    public Duration getParseTime() {
        return parseTime;
    }

    public void setParseTime(Duration parseTime) {
        this.parseTime = parseTime;
    }

    public Duration getGlobalTime() {
        return globalTime;
    }

    public void setGlobalTime(Duration globalTime) {
        this.globalTime = globalTime;
    }

    public LocalDateTime getDateTimeStart() {
        return dateTimeStart;
    }

    public void setDateTimeStart(LocalDateTime dateTimeStart) {
        this.dateTimeStart = dateTimeStart;
    }

    public LocalDateTime getDateTimeScanFinish() {
        return dateTimeScanFinish;
    }

    public void setDateTimeScanFinish(LocalDateTime dateTimeScanFinish) {
        this.dateTimeScanFinish = dateTimeScanFinish;
    }

    public LocalDateTime getDateTimeParseFinish() {
        return dateTimeParseFinish;
    }

    public void setDateTimeParseFinish(LocalDateTime dateTimeParseFinish) {
        this.dateTimeParseFinish = dateTimeParseFinish;
    }

    public LocalDateTime getDateTimeParseStart() {
        return dateTimeParseStart;
    }

    public void setDateTimeParseStart(LocalDateTime dateTimeParseStart) {
        this.dateTimeParseStart = dateTimeParseStart;
    }

    public boolean isScanResultParsed() {
        return isScanResultParsed;
    }

    public void setScanResultParsed(boolean scanResultParsed) {
        isScanResultParsed = scanResultParsed;
    }

    public boolean isScanResultScanned() {
        return isScanResultScanned;
    }

    public void setScanResultScanned(boolean scanResultScanned) {
        isScanResultScanned = scanResultScanned;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<MCFile> getFoundMCFilesList() {
        return foundMCFilesList;
    }

    public void setFoundMCFilesList(List<MCFile> foundMCFilesList) {
        this.foundMCFilesList = foundMCFilesList;
    }

    public List<MCFile> getExcludeMCFilesList() {
        return excludeMCFilesList;
    }

    public void setExcludeMCFilesList(List<MCFile> excludeMCFilesList) {
        this.excludeMCFilesList = excludeMCFilesList;
    }

    public List<String> getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(List<String> messagesList) {
        this.messagesList = messagesList;
    }

    public String getId() {
        return id;
    }

    public String getScanId() {
        return scanId;
    }
}
