package org.home.scanner;

import org.home.entity.mediaCollection.MediaCollection;

public class ScanResultDifference {

    private int id;

    private MediaCollection mediaCollection;
    private ScanResult scanResult;

    public ScanResultDifference(MediaCollection mediaCollection, ScanResult sc) {
        this.mediaCollection = mediaCollection;
        this.scanResult = sc;
    }

    public MediaCollection compare(){
        //TODO implement comparing
        return mediaCollection;
    }

    public int getId() {
        return id;
    }
}
