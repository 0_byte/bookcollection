package org.home.db;

import java.util.Arrays;
import java.util.List;

public enum DbSaverType {

    SQLITE_SAVER ("sqlite"),
    MYSQL_SAVER ("mysq;"),
    POSTRGRES_SAVER ("postrgres");

    private final String type;

    private DbSaverType(final  String  type){
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public List<DbSaverType> getDbSaversList(){
        return Arrays.asList(DbSaverType.values());
    }
}
