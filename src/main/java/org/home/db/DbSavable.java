package org.home.db;

public interface DbSavable {

    public String getConnectionURL();

    public DbSaverType getSaverType();

    public boolean save(String id, Object obg);

    public boolean update(String id, Object obj);

    public Object getObjById(String tblName, String id);

    public boolean removeObjById(String id);




}
