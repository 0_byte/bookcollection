package org.home.db.savers;

import org.home.db.DbSavable;
import org.home.db.DbSaverType;

public class SqlLiteSaver implements DbSavable{

    @Override
    public String getConnectionURL() {
        return null;
    }

    @Override
    public DbSaverType getSaverType() {
        return null;
    }

    @Override
    public boolean save(String id, Object obg) {
        return false;
    }

    @Override
    public boolean update(String id, Object obj) {
        return false;
    }

    @Override
    public Object getObjById(String tblName, String id) {
        return null;
    }

    @Override
    public boolean removeObjById(String id) {
        return false;
    }
}
