package org.home.entity.mediaCollection;

import java.util.Arrays;
import java.util.List;

public enum FileType {
    FILE_TYPE_ATTACH("attach"),
    FILE_TYPE_UNREADABLE("unreadable"),

    /*  file that is not persist in config 'extension' param, already scanned,
        but can be an attach for a book file in a future, like source code for a book or other useful
        document */
    FILE_TYPE_UNDEFINED("undefined"),

    /*  files types that persist in config 'extension' param, already scanned, but not parsed yet */
    FILE_TYPE_READABLE("readable_but_not_parsed");

    private final String type;

    private FileType(String type) {
        this.type = type;
    }

    public static List<FileType> getListOfValues() {
        return Arrays.asList(FileType.values());
    }

    @Override
    public String toString() {
        return type;
    }
}
