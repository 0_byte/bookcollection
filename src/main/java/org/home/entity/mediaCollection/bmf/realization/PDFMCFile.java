package org.home.entity.mediaCollection.bmf.realization;

import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.bmf.BaseMCFile;
import org.home.entity.mediaCollection.bmf.MCFilable;

import java.util.List;

public class PDFMCFile extends BaseMCFile implements MCFilable {

    private int numberOfPages;
    private String subject;
    private String creator;
    private String producer;
    private String version;
    private String createDate;
    private String modifDate;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifDate() {
        return modifDate;
    }

    public void setModifDate(String modifDate) {
        this.modifDate = modifDate;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Override
    public BookFileType getBookFileType() {
        return BookFileType.FILE_TYPE_PDF;
    }
}
