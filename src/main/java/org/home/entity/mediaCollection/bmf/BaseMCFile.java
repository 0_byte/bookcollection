package org.home.entity.mediaCollection.bmf;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public abstract class BaseMCFile {

    protected String bookName;
    protected String title;
    protected String author;

    protected String description;
    protected String language;

    protected boolean haveCover;
    protected String previewImageFile;

    protected String ISBN;
    protected List<String> keywords;

    public void setKeywords(String keywords){
        this.keywords = Arrays.asList(keywords.split(Pattern.quote(";")));
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookName() { return bookName;}

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isHaveCover() {
        return haveCover;
    }

    public void setHaveCover(boolean haveCover) {
        this.haveCover = haveCover;
    }

    public String getPreviewImageFile() {
        return previewImageFile;
    }

    public void setPreviewImageFile(String previewImageFile) {
        this.previewImageFile = previewImageFile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
