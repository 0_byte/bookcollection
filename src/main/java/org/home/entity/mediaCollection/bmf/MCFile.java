package org.home.entity.mediaCollection.bmf;

import org.home.entity.mediaCollection.FileType;
import org.home.utils.UniquePCId;

import java.nio.file.Path;
import java.nio.file.attribute.FileTime;

public class MCFile {

    private String id;
    private String mediaCollectionId;

    private Path file;
    private String fileName;
    private String ext;
    private FileTime modifDate;
    private String size;
    private boolean isReadable;
    private boolean isExists;
    private boolean isFile; // or folder
    private boolean nonBookFile = false;
    private FileType fileType;

    private MCFilable MCfile;

    public MCFile(String mediaCollectionId, Path file) {
        this.file = file;
        this.id = generateUid();
        this.mediaCollectionId = mediaCollectionId;
        this.fileType = FileType.FILE_TYPE_UNDEFINED; // unknown file type
    }

    public MCFile(String mediaCollectionId, Path file, FileType fileType) {
        this.file = file;
        this.id = generateUid();
        this.mediaCollectionId = mediaCollectionId;
        this.fileType = fileType;
    }

    private MCFile() {}

    private String generateUid(){
        return UniquePCId.hash(this.file.toAbsolutePath().toString());
    }

    @Override
    public String toString() {
        return "Mc file{" +
                "id: " + id +
                "; mc id: " + mediaCollectionId +
                "; file path: " + file.toString() +
                "; file Name: " + fileName +
                "; ext: " + ext +
                "; is Readable: " + isReadable +
                "; is exists: " + isExists +
                "; is file: " + isFile +
                "; nonBook File: " + nonBookFile +
                "; size: " + size +
                "; modif Date: " + modifDate.toString();
    }

    public MCFilable getMCfile() {
        return MCfile;
    }

    public void setMCfile(MCFilable MCfile) {
        this.MCfile = MCfile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMediaCollectionId() {
        return mediaCollectionId;
    }

    public void setMediaCollectionId(String mediaCollectionId) {
        this.mediaCollectionId = mediaCollectionId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public Path getFile() {
        return file;
    }

    public void setFile(Path file) {
        this.file = file;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isReadable() {
        return isReadable;
    }

    public void setIsReadable(boolean is_readable) {
        this.isReadable = is_readable;
    }

    public boolean isExists() {
        return isExists;
    }

    public void setIsExists(boolean exists) {
        this.isExists = exists;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setIsFile(boolean file) {
        this.isFile = file;
    }

    public FileTime getModifDate() {
        return modifDate;
    }

    public void setModifDate(FileTime modifDate) {
        this.modifDate = modifDate;
    }

    public boolean isNonBookFile() {
        return nonBookFile;
    }

    public void setNonBookFile(boolean nonBookFile) {
        this.nonBookFile = nonBookFile;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }
}


