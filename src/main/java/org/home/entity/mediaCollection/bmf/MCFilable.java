package org.home.entity.mediaCollection.bmf;

import org.home.entity.mediaCollection.BookFileType;

public interface MCFilable {

    public BookFileType getBookFileType();
}
