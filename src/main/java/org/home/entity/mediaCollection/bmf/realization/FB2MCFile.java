package org.home.entity.mediaCollection.bmf.realization;

import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.bmf.BaseMCFile;
import org.home.entity.mediaCollection.bmf.MCFilable;

public class FB2MCFile extends BaseMCFile implements MCFilable {

    private String encoding;

    private String annotation;
    private String genre;
    private String translator;
    private String creationDate;

    private String publisherBookName;
    private String publisherName;
    private String publisherCity;
    private String publisherYear;
    private String publisherIsbn;

    private String srcLang;
    private String srcGenre;
    private String srcAuthor;
    private String srcTitle;
    private String srcAnnotation;
    private String srcKeywords;
    private String srcDate;


    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getEncoding() {return encoding;}

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

    public String getSrcLang() {
        return srcLang;
    }

    public void setSrcLang(String srcLang) {
        this.srcLang = srcLang;
    }

    public String getSrcGenre() {
        return srcGenre;
    }

    public void setSrcGenre(String srcGenre) {
        this.srcGenre = srcGenre;
    }

    public String getSrcAuthor() {
        return srcAuthor;
    }

    public void setSrcAuthor(String srcAuthor) {
        this.srcAuthor = srcAuthor;
    }

    public String getSrcTitle() {
        return srcTitle;
    }

    public void setSrcTitle(String srcTitle) {
        this.srcTitle = srcTitle;
    }

    public String getSrcAnnotation() {
        return srcAnnotation;
    }

    public void setSrcAnnotation(String srcAnnotation) {
        this.srcAnnotation = srcAnnotation;
    }

    public String getSrcKeywords() {
        return srcKeywords;
    }

    public void setSrcKeywords(String srcKeywords) {
        this.srcKeywords = srcKeywords;
    }

    public String getSrcDate() {
        return srcDate;
    }

    public void setSrcDate(String srcDate) {
        this.srcDate = srcDate;
    }

    public String getPublisherBookName() {
        return publisherBookName;
    }

    public void setPublisherBookName(String publisherBookName) {
        this.publisherBookName = publisherBookName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getPublisherCity() {
        return publisherCity;
    }

    public void setPublisherCity(String publisherCity) {
        this.publisherCity = publisherCity;
    }

    public String getPublisherYear() {
        return publisherYear;
    }

    public void setPublisherYear(String publisherYear) {
        this.publisherYear = publisherYear;
    }

    public String getPublisherIsbn() {
        return publisherIsbn;
    }

    public void setPublisherIsbn(String publisherIsbn) {
        this.publisherIsbn = publisherIsbn;
    }

    @Override
    public BookFileType getBookFileType() {
        return BookFileType.FILE_TYPE_FB2;
    }

}

