package org.home.entity.mediaCollection.bmf.realization;

import org.home.entity.mediaCollection.BookFileType;
import org.home.entity.mediaCollection.bmf.BaseMCFile;
import org.home.entity.mediaCollection.bmf.MCFilable;

import java.util.List;

public class EpubMCFile extends BaseMCFile implements MCFilable{

    private String publisherName;

    public String getPublisherName() {
        return publisherName;
    }
    private List<String> idenifierList;
    private List<String> dateList;

    public List<String> getDateList() {
        return dateList;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public List<String> getIdenifierList() {
        return idenifierList;
    }

    public void setIdenifierList(List<String> idenifierList) {
        this.idenifierList = idenifierList;
    }

    @Override
    public BookFileType getBookFileType() {
        return BookFileType.FILE_TYPE_EPUB;
    }
}
