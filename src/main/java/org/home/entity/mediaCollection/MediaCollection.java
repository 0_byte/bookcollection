package org.home.entity.mediaCollection;

import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.utils.FileUtils;
import org.home.utils.UniquePCId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;

public class MediaCollection {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    private String id;
    private Path rootPath;
    private Device parent;
    private FileTime modifDate;
    private boolean is_readable;
    private boolean is_exists;
    private boolean is_folder;
    private boolean is_file;

    private List<MCFile> mediaCollectionList;
    private List<MCFile> excludeMCFilesList;
    private List<MCFile> excludeMCFoldersList;
    private List<MCFile> missedMCFilesList;

    private MediaCollection(){}

    public MediaCollection(Device parent, Path rootPath) {
        id = this.calculateId(parent, rootPath);
        this.parent = parent;
        this.rootPath = rootPath;
        this.mediaCollectionList = new ArrayList<>();
        this.excludeMCFilesList = new ArrayList<>();
        this.excludeMCFoldersList = new ArrayList<>();
        this.missedMCFilesList = new ArrayList<>();
    }

    private String calculateId(Device parent, Path rootPath){
        return parent.getId() + "-"
                + UniquePCId.hash(rootPath.toAbsolutePath().toString());
    }

    public void add(MCFile mcFile) {
        this.mediaCollectionList.add(mcFile);
    }

    public List<MCFile> getMediaCollectionList() {
        return mediaCollectionList;
    }

    public void addExcluded(MCFile mcFile) {
        this.excludeMCFilesList.add(mcFile);
    }

    public void addMissed(MCFile mcFile) {
        this.missedMCFilesList.add(mcFile);
    }

    public List<MCFile> getExcludeMCFilesList() {
        return excludeMCFilesList;
    }

    public List<MCFile> getMissedMCFilesList() {
        return missedMCFilesList;
    }

    public void setExcludeMCFilesList(List<MCFile> excludeMCFilesList) {
        this.excludeMCFilesList = excludeMCFilesList;
    }

    public void setMissedMCFilesList(List<MCFile> missedMCFilesList) {
        this.missedMCFilesList = missedMCFilesList;
    }

    public Device getParent() {
        return parent;
    }

    public String getId() {
        return id;
    }

    public Path getRootPath() {
        return rootPath;
    }

    public FileTime getModifDate() {
        return modifDate;
    }

    public void setModifDate(FileTime modifDate) {
        this.modifDate = modifDate;
    }


    public boolean is_exists() {
        return is_exists;
    }

    public void setIs_exists(boolean is_exists) {
        this.is_exists = is_exists;
    }

    public boolean is_readable() {
        return is_readable;
    }

    public void setIs_readable(boolean is_readable) {
        this.is_readable = is_readable;
    }

    public boolean is_folder() {
        return is_folder;
    }

    public void setIs_folder(boolean is_folder) {
        this.is_folder = is_folder;
    }

    public boolean is_file() {
        return is_file;
    }

    public void setIs_file(boolean is_file) {
        this.is_file = is_file;
    }

    public List<MCFile> getExcludeMCFoldersList() {
        return excludeMCFoldersList;
    }

    public void setExcludeMCFoldersList(List<MCFile> excludeMCFoldersList) {
        this.excludeMCFoldersList = excludeMCFoldersList;
    }
}
