package org.home.entity.mediaCollection;

import java.util.Arrays;
import java.util.List;

public enum BookFileType {
    FILE_TYPE_PDF("pdf"),
    FILE_TYPE_FB2("fb2"),
    FILE_TYPE_EPUB("epub"),
    FILE_TYPE_TXT("txt"),
    FILE_TYPE_DOC("doc"),
    FILE_TYPE_RTF("rtf"),
    FILE_TYPE_BOOK_IMPLEMENTATION("undefined file format"); //in case of new file type

    private final String type;

    private BookFileType(final String type) {
        this.type = type;
    }

    public static List<BookFileType> getListOfValues() {
        return Arrays.asList(BookFileType.values());
    }

    @Override
    public String toString() {
        return type;
    }
}
