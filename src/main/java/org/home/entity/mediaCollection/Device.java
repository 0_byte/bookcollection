package org.home.entity.mediaCollection;

import org.home.utils.UniquePCId;

import java.nio.file.Path;
import java.util.Properties;

public class Device {
    private String id;
    private String name;
    private Path rootPath;
    private String description;

    private String homeDir;
    private String userName;
    private String osName;
    private String osArch;
    private String osVersion;

    public Device() {
        this.init();
    }

    private void init() {
        id = new UniquePCId().getPCId();
        homeDir = System.getenv().get("HOME");

        Properties properties = System.getProperties();
        userName = properties.getProperty("user.name");
        osName = properties.getProperty("os.name");
        osArch = properties.getProperty("os.arch");
        osVersion = properties.getProperty("os.version");
    }

    @Override
    public String toString() {
        return "Device{" +
                "homeDir='" + homeDir + '\'' +
                ", userName='" + userName + '\'' +
                ", osName='" + osName + '\'' +
                ", osArch='" + osArch + '\'' +
                ", osVersion='" + osVersion + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Path getRootPath() {
        return rootPath;
    }

    public void setRootPath(Path rootPath) {
        this.rootPath = rootPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getHomeDir() {
        return homeDir;
    }

    public String getUserName() {
        return userName;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsArch() {
        return osArch;
    }

    public String getOsVersion() {
        return osVersion;
    }
}
