package org.home.entity.mediaCollection;

import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.entity.mediaCollection.bmf.realization.EpubMCFile;
import org.home.entity.mediaCollection.bmf.realization.FB2MCFile;
import org.home.entity.mediaCollection.bmf.realization.PDFMCFile;
import org.home.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MCUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static boolean isMCFilePDF(MCFile mcFile){
        boolean isPDF = mcFile.getMCfile() instanceof PDFMCFile;
        logger.debug("Checking if file is pdf instance: " + isPDF);
        return isPDF;
    }

    public static boolean isMCFileFb2(MCFile mcFile){
        boolean isFb2 = mcFile.getMCfile() instanceof FB2MCFile;
        logger.debug("Checking if file is fb2 instance: " + isFb2);
        return isFb2;
    }

    public static boolean isMCFileEpub(MCFile mcFile){
        boolean isEpub = mcFile.getMCfile() instanceof EpubMCFile;
        logger.debug("Checking if file is epub instance: " + isEpub);
        return isEpub;
    }

    public static PDFMCFile castToPDF(MCFile mcFile){
        PDFMCFile pdfmcFile = null;
        if (MCUtils.isMCFilePDF(mcFile)){
            try{
                pdfmcFile = (PDFMCFile) mcFile.getMCfile();
            }catch (ClassCastException cce){
                logger.error("Cannot cast to pdf instance " + cce.getMessage());
            }
        }

        return pdfmcFile;
    }

    public static boolean isExcludeFile(MediaCollection mediaCollection, Path file) throws IOException {
        boolean isContains = false;
        for (MCFile mcFile: mediaCollection.getExcludeMCFilesList()){
            if(Files.isSameFile(mcFile.getFile(), file)){
                isContains = true;
                break;
            }
        }
        return isContains;
    }

    public static boolean isExcludeFolder(MediaCollection mediaCollection, Path folder) throws IOException {
        boolean isContains = false;
        for (MCFile mcFile: mediaCollection.getExcludeMCFilesList()){
            if(Files.isSameFile(mcFile.getFile(), folder)){
                isContains = true;
                break;
            }
        }
        return isContains;
    }
}
