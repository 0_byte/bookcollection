package org.home.entity;

import org.home.entity.mediaCollection.bmf.MCFile;

import java.util.List;

public class Book {

    private int id;
    private String title;
    private Author author;
    private Publisher publisher;

    private List<MCFile> bookFiles;

    public Book() {}

    public Book(List<MCFile> bookFiles) {
        this.bookFiles = bookFiles;
    }

    public void addBookFile(MCFile mcFile){
        this.bookFiles.add(mcFile);
    }

    public List<MCFile> getBookFiles() {
        return bookFiles;
    }

    public void setBookFiles(List<MCFile> bookFiles) {
        this.bookFiles = bookFiles;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
}
