package org.home.parser;


import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.entity.mediaCollection.bmf.realization.PDFMCFile;
import org.home.scanner.Scanner;
import org.home.utils.PropertiesHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PDFParser {
    private static final Logger logger =
            LoggerFactory.getLogger(PDFParser.class);
    private static String temp_directory =
            PropertiesHandler.getPropertiesHanlder().getProperty("temp_directory");

    public static PDFMCFile parse(MCFile mcFile, boolean grabCover) throws IOException {

        logger.trace(Scanner.separator );
        logger.trace("Parsing " + mcFile.getFileName());
        Path file = mcFile.getFile();
        PDFMCFile pdfmcFile = new PDFMCFile();

        org.apache.pdfbox.pdfparser.PDFParser parser =
                new org.apache.pdfbox.pdfparser.PDFParser(
                        new RandomAccessFile(file.toFile(), "rw"));
        parser.parse();

        PDDocument document = parser.getPDDocument();
        if (document.isEncrypted()) {
            logger.error("Document is encrypted");
            return pdfmcFile;
        }

        PDDocumentInformation info = document.getDocumentInformation();

        pdfmcFile.setBookName(file.getFileName().toString());

        pdfmcFile.setTitle(info.getTitle());
        pdfmcFile.setAuthor(info.getAuthor());
        pdfmcFile.setNumberOfPages(document.getNumberOfPages());
        pdfmcFile.setSubject(info.getSubject());

        pdfmcFile.setCreator(info.getCreator());
        pdfmcFile.setProducer(info.getProducer());
        pdfmcFile.setKeywords(info.getKeywords());

        pdfmcFile.setLanguage(document.getDocumentCatalog().getLanguage());
        pdfmcFile.setVersion(document.getDocumentCatalog().getVersion());

        SimpleDateFormat format = new SimpleDateFormat(
                PropertiesHandler.getPropertiesHanlder().getProperty("date_format"));
        Calendar creationDate = info.getCreationDate();
        if (creationDate != null) {
            pdfmcFile.setCreateDate(format.format(creationDate.getTime()));
        } else {
            logger.trace("empty creation date");
        }

        Calendar modificationDate = info.getModificationDate();
        if (modificationDate != null) {
            pdfmcFile.setModifDate(format.format(modificationDate.getTime()));
        } else {
            logger.trace("empty modification date");
        }

        if(grabCover){
            //generating preview image and save it to temporary directory
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            BufferedImage bufferedImage = pdfRenderer.renderImageWithDPI(0, 50, ImageType.RGB);

            pdfmcFile.setPreviewImageFile(mcFile.getMediaCollectionId() + "-" + mcFile.getId() + ".png");

            ImageIOUtil.writeImage(bufferedImage, temp_directory + File.separator
                            + pdfmcFile.getPreviewImageFile(), 50);

            logger.trace("Book successfully parsed");
            pdfmcFile.setHaveCover(true);
        }

        document.close();
        return pdfmcFile;
    }
}
