package org.home.parser;

import nl.siegmann.epublib.domain.Author;
import nl.siegmann.epublib.domain.Date;
import nl.siegmann.epublib.domain.Identifier;
import nl.siegmann.epublib.domain.Metadata;
import nl.siegmann.epublib.epub.EpubReader;
import org.home.entity.mediaCollection.bmf.MCFile;
import org.home.entity.mediaCollection.bmf.realization.EpubMCFile;
import org.home.scanner.Scanner;
import org.home.utils.PropertiesHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EpubParser {
    private static final Logger logger =
            (Logger)  LoggerFactory.getLogger(EpubParser.class);
    private static String temp_directory =
            PropertiesHandler.getPropertiesHanlder().getProperty("temp_directory");

    public static EpubMCFile parse(MCFile mcFile, boolean grabCover) throws IOException {
        EpubReader epubReader = new EpubReader();
        logger.trace(Scanner.separator );
        logger.trace("parsing " + mcFile.getFileName());
        EpubMCFile epubMCFile = new EpubMCFile();

        nl.siegmann.epublib.domain.Book epubBook
                = epubReader.readEpub(new FileInputStream(mcFile.getFile().toFile()));
        StringBuilder sb = new StringBuilder();

        Metadata metadata = epubBook.getMetadata();
        String format = metadata.getFormat();
        logger.trace("epub format " + format);

        List<Author> authors = metadata.getAuthors();
        if(authors.size() > 0){
            authors.forEach(author -> {
                sb.append(author.getFirstname()).append(" ")
                        .append(author.getLastname()).append(";");
                logger.trace("Found author name " + author.getFirstname() + " " + author.getLastname());
            });
            epubMCFile.setAuthor(sb.toString());
        }

        if(metadata.getDescriptions().size() > 0){
            sb.setLength(0);
            metadata.getDescriptions().forEach(descr->{
                logger.trace("Description found " + descr);
                sb.append(descr).append(";");
            });
            epubMCFile.setDescription(sb.toString());
        }

        if(metadata.getIdentifiers().size() > 0){
            ArrayList<String> identifiersList = new ArrayList<>();
            logger.trace("Found identifier");
            for(Identifier identifier :metadata.getIdentifiers()){
                logger.trace(identifier.getScheme() + " : " + identifier.getValue());
                identifiersList.add(identifier.getValue());
            }
            epubMCFile.setIdenifierList(identifiersList);
        }

        List<Date> dates = metadata.getDates();
        if(dates.size() > 0){
            ArrayList<String> dateList = new ArrayList<>();
            logger.trace("found date");
            for(Date date:dates){
                logger.trace(date.toString());
                dateList.add(date.toString());
            }
            epubMCFile.setDateList(dateList);
        }

        if(metadata.getPublishers().size() > 0 ){
            sb.setLength(0);
            metadata.getPublishers().forEach(pub -> {
                logger.trace("Publisher " + pub);
                sb.append(pub);
            });
            epubMCFile.setPublisherName(sb.toString());
        }

        logger.trace("Lang " + metadata.getLanguage());
        epubMCFile.setLanguage(metadata.getLanguage());

        logger.trace("Book title" + epubBook.getTitle());
        epubMCFile.setTitle(epubBook.getTitle());

        metadata.getOtherProperties().forEach((qName, s) -> {
            logger.trace("prop " + qName.toString() + " " + s);
        });


        if(grabCover){
            logger.trace("Attempt to receive cover image");
            if(epubBook.getCoverImage() != null){
                logger.trace("Cover image exists");
                epubMCFile.setPreviewImageFile(mcFile.getMediaCollectionId() + "-" + mcFile.getId() + ".jpg");
                epubMCFile.setHaveCover(true);
                byte[] imageData = epubBook.getCoverImage().getData();
                BufferedImage read = ImageIO.read(new ByteArrayInputStream(imageData));

                ImageIO.write(read, "jpg", new File(
                        temp_directory + File.separator
                                + epubMCFile.getPreviewImageFile()));
            }
        }

        return epubMCFile;
    }
}
