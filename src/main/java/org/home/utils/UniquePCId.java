package org.home.utils;

import com.sun.istack.internal.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Collectors;

public class UniquePCId {
    private Properties properties;
    private String pcId;
    private int processorsNumber;
    private long totalRootSpace;
    private String osName;
    private String arch;
    private String cpuInfo;
    private String memInfo;
    private String machineInfo;

    public UniquePCId() {
        properties = System.getProperties();
        osName = properties.getProperty("os.name");

        try {
            switch (osName.toLowerCase()) {
                case "linux":
                    getLinuxProperties();
            }
        } catch (IOException e) {
            setAnotherPcId();
        } finally {
            calculatePcIdHash();
        }
    }

    public String getPCId() {
        return pcId;
    }

    private void calculatePcIdHash() {
        pcId = hash(setAnotherPcId()
                + processorsNumber
                + totalRootSpace
                + arch
                + cpuInfo
                + memInfo
                + machineInfo);
    }

    public static String hash(String str) {
        // Standard implementation of hash String method.
        // This code belong to this place in case if hash method would change in the future
        int h = 0;
        char[] charArray = str.toCharArray();
        if (charArray.length > 0) {
            for (char aValue : charArray) {
                h = 31 * h + aValue;
            }
        }
        return String.valueOf(Math.abs(h));
    }

    private String setAnotherPcId() {
        return properties.getProperty("user.name")
                + osName
                + properties.getProperty("os.arch")
                + properties.getProperty("os.version");
    }

    private void getLinuxProperties() throws IOException {
        processorsNumber = Runtime.getRuntime().availableProcessors();

        //TODO find optimal solution for get() method, kinda 'shitcode' style now
        totalRootSpace = Arrays.stream(File.listRoots())
                .findFirst().get().getTotalSpace();

        arch = new BufferedReader(
                new InputStreamReader(
                        Runtime.getRuntime().exec("uname -m")
                                .getInputStream()))
                .lines().findFirst().orElse("none");
        cpuInfo = new BufferedReader(new InputStreamReader(
                Runtime.getRuntime().exec("cat /proc/cpuinfo")
                        .getInputStream()))
                .lines().filter(s -> s.startsWith("model name")).findFirst().orElse("none");

        memInfo = new BufferedReader(new InputStreamReader(
                Runtime.getRuntime().exec("cat /proc/meminfo")
                        .getInputStream()))
                .lines().filter(s -> s.startsWith("MemTotal")).findFirst().orElse("none");

        //TODO test this on various machines
        machineInfo =
                new BufferedReader(new InputStreamReader(
                        Runtime.getRuntime().exec("inxi -M -c 0")
                                .getInputStream()))
                        .lines().collect(Collectors.joining()).replaceAll("\\s+", "");
    }
}
