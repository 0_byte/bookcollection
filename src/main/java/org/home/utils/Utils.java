package org.home.utils;

import org.home.scanner.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;


public class Utils {
    private static final Logger logger =
            LoggerFactory.getLogger(Utils.class);

    public static StringWriter printStackTrace(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw;
    }

    public static String timeFormatter(Duration duration){
        long seconds = duration.getSeconds();

        long hours = 0, minutes = 0;
        long secs = seconds % 60;

        if (seconds > 60){
            minutes = ((duration.getSeconds() % (60 * 60)) / 60);
        }
        if(seconds > 3600){
            hours = duration.toHours();
        }
        return hours + " h " + minutes + " m " + secs + "s";
    }
}
