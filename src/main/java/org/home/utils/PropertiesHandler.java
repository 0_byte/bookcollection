package org.home.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PropertiesHandler {
    private static final Logger logger =
            LoggerFactory.getLogger(PropertiesHandler.class);
    private static final String propFile = "config.properties";

    private static Properties properties = null;
    private static PropertiesHandler propertiesHandler = null;

    private PropertiesHandler() {}

    public static PropertiesHandler getPropertiesHanlder(){
        if(propertiesHandler == null){
            propertiesHandler = new PropertiesHandler();
            properties = new Properties();

            if(properties.isEmpty()){
                propertiesHandler.readProperties();
            }

            return propertiesHandler;
        }else {
            return propertiesHandler;
        }
    }

    public void readProperties(){
        InputStream inputStream = null;
        try {

            inputStream = ClassLoader.getSystemResourceAsStream(propFile);
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            logger.error("Properties file not found");
        } catch (IOException e) {
            logger.error("Cannot read properties file content");
        }finally {
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error("Cannot close properties stream");
                }
            }
        }
    }

    public void saveProperties(){
        OutputStream outputStream = null;
        try{
            outputStream= new FileOutputStream("src" + File.separator + "main"
                    + File.separator + "resources" + File.separator + propFile);
            properties.store(outputStream, null);
            logger.debug("saved");
        } catch (FileNotFoundException e) {
            logger.error("Properties file not found");
        } catch (IOException e) {
            logger.error("Cannot save properties file content");
        }finally {
            if(outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error("Cannot close properties stream");
                }
            }
        }
    }

    public void saveProperty(String key, String value){
        properties.setProperty(key, value);
        saveProperties();
    }

    public String getProperty(String key){
        return properties.getProperty(key);
    }

    public List<String> getFileExtensions(){
        return Arrays.asList(properties
                .getProperty("extension")
                .split(","));
    }

}
