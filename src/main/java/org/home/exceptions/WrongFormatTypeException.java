package org.home.exceptions;

public class WrongFormatTypeException extends Exception {
    public WrongFormatTypeException() {
        super();
    }

    public WrongFormatTypeException(String message) {
        super(message);
    }

    public WrongFormatTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongFormatTypeException(Throwable cause) {
        super(cause);
    }
}
