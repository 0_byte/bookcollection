Book Collection
===============

E-book organization library.

Specifically this lib is a parser for obtaining book metadata
for subsequent data saving in db storage.

Tou can scan specified directories and then parse files if they are books. 
Scanner can have exclude files and folder, file format filters.

Parser obtain book metadata and cover (if this setting was specified).
Book Collection can parse those popular e-book formats:
 * pdf
 * epub
 * fb2

In current version all scan/parse results saves to sqlLite db for further use.
Data storage as media collection for device, where scan was produced.
You can synchronize this information between devices (future plans).
Each scan/parse action have own session and status.
Each book can have few file implementations, like pdf and fb2 for same book or few editions.



